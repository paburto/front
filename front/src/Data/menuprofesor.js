export default [
{
    title: 'Inicio',
    url:'/'
},
{
    title: 'Alumnos',
    url:'/alumnos'
},
{
    title: 'Crear Pruebas',
    url:'/crearpruebas'
},
{
    title: 'Pruebas',
    url:'/pruebas'
},

];