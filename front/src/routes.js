//Dependencies
import React from 'react';
import { Route } from 'react-router-dom';

//Components

import App from './Componentes/App';
import Login from './Componentes/Login';
import CrearPrueba from './Componentes/CrearPrueba';
import Inicio from './Componentes/Inicio';
import Pruebas from './Componentes/Pruebas';
import Alumnos from './Componentes/Alumnos';

const AppRoutes = () =>
    <App>
      
      <Route path = "/login" exact={true} component = {Login}/>
      <Route path = "/crearpruebas" exact={true} component = {CrearPrueba}/>
      <Route path = "/alumnos" exact={true} component = {Alumnos}/>
      <Route path = "/" exact={true} component = {Inicio}/>
      <Route path = "/pruebas" exact={true} component = {Pruebas}/>

    </App>;

export default AppRoutes;