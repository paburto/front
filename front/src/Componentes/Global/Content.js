import React, { Component } from 'react';
import './css/Content.css';

class Content extends Component {
	constructor(){
		super();

		this.state={
		
		}

	}


  render() {
    const {body} = this.props;
    return (
      <div className="Content">
        {body}
      </div>
    );
  }
}

export default Content;
