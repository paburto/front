//Dependencies
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import cookie from 'react-cookies';
import axios from 'axios';

//Assets
import './css/Header.css';
import MenuA from '../Data/MenuAdmin.js';
import MenuB from '../Data/MenuTherapist.js';
