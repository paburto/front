import React, {Component} from 'react';
import './Global/css/login.css';

class Login extends Component {
	render(){
		return (
			<div className="row small-up-2 medium-up-3 	large-up-4">
				<div className="column">
					<h2>Iniciar Sesión</h2>
					<label>Usuario</label>
					<input type="text" name="usuario" placeholder="username"/>
					<label>Contraseña</label>
					<input type="password" name="contraseña" placeholder="password"/>

					<hr/><br/>
				</div>
			</div>
		)

	}
}

export default Login