//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

//Components
import Header from './Global/Header';
import Content from './Global/Content';
import Footer from './Global/Footer';
import Login from './Login';

//Data
import items from '../Data/menuprofesor';
import item from '../Data/menualumno';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };
  render() {
    const { children } = this.props;
     return (
      <div className="App">
        <Header title= "Fundamentos de programación" items={items}/>
        <Content body = {children}/>
        <Footer copyright = "&copy;Universidad Santiago de Chile"/>
      </div>
    );
  }
}

export default App;
