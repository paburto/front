//Dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Componentes/App';
import * as serviceWorker from './serviceWorker';
import AppRoutes from './routes';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';


render(
    <Router>
    	<AppRoutes/>
    </Router>,
  document.getElementById('root')
);

serviceWorker.unregister();
